﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static async Task CalculateFactorialAsync()
        {
            Console.WriteLine("\nFactorial Thread ID (Before await): " + Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Factorial Thread Name (Before await): " + Thread.CurrentThread.Name);
            Console.WriteLine("Factorial Thread IsThreadPoolThread (Before await): " + Thread.CurrentThread.IsThreadPoolThread);

            int result = await Task.Run(() =>
            {
                Console.WriteLine("\nFactorial Thread ID (Inside Task): " + Thread.CurrentThread.ManagedThreadId);
                Console.WriteLine("Factorial Thread Name (Inside Task): " + Thread.CurrentThread.Name);
                Console.WriteLine("Factorial Thread IsThreadPoolThread (Inside Task): " + Thread.CurrentThread.IsThreadPoolThread);

                return Factorial(5);
            }).ConfigureAwait(true);

            Console.WriteLine("\nFactorial Thread ID (After await): " + Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Factorial Thread Name (After await): " + Thread.CurrentThread.Name);
            Console.WriteLine("Factorial Thread IsThreadPoolThread (After await): " + Thread.CurrentThread.IsThreadPoolThread);

            Console.WriteLine("\nFactorial Result: " + result);
        }

        static int Factorial(int num)
        {
            int result = 1;
            for (int i = num; i > 0; i--)
            {
                result *= i;
            }
            Thread.Sleep(2000);
            return result;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Main Thread ID: " + Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Main Thread Name: " + Thread.CurrentThread.Name);
            Console.WriteLine("Main Thread IsThreadPoolThread: " + Thread.CurrentThread.IsThreadPoolThread);

            CalculateFactorialAsync().Wait();

            Console.ReadKey();
        }
    }
}
