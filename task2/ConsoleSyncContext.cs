﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace task2
{
    internal class ConsoleSyncContext : SynchronizationContext
    {
        public override void Post(SendOrPostCallback d, object state)
        {
            Thread thread = new Thread(() =>
            {
                SetSynchronizationContext(this);
                d(state);
            });

            thread.Name = "CustomThread";
            thread.Start();
        }
    }
}
