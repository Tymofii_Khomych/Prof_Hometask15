﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace task5
{
    class AsyncVoidSyncContext : SynchronizationContext
    {
        public override void Post(SendOrPostCallback d, object state)
        {
            try
            {
                d(state);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in async void method: {ex.Message}");
            }
        }
    }

    class Program
    {
        static async void AsyncMethod()
        {
            await Task.Delay(1000);
            throw new Exception("Error in async void method");
        }

        static void Main(string[] args)
        {
            var syncContext = new AsyncVoidSyncContext();
            SynchronizationContext.SetSynchronizationContext(syncContext);

            Console.WriteLine("Main Thread ID: " + Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Main Thread Name: " + Thread.CurrentThread.Name);
            Console.WriteLine("Main Thread IsThreadPoolThread: " + Thread.CurrentThread.IsThreadPoolThread);

            AsyncMethod();

            Console.ReadKey();
        }
    }
}
