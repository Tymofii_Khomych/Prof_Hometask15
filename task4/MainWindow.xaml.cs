﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace task4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public int Addition(int a, int b)
        {
            return a + b;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            int num1 = 10;
            int num2 = 5;

            int result = await Task.Run(() => Addition(num1, num2)).ConfigureAwait(false);
            label.Content = result;
        }
    }
}
